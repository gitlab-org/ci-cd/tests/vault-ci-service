ARG VAULT_VERSION

FROM vault:${VAULT_VERSION}

COPY build/vaultcs /usr/local/bin

ENTRYPOINT []
CMD ["vaultcs"]
