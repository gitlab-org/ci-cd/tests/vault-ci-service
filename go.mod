module gitlab.com/gitlab-org/ci-cd/tests/vault-ci-service

go 1.12

require (
	github.com/docker/distribution v2.7.1+incompatible // indirect
	github.com/docker/docker v0.0.0-20190408150954-50ebe4562dfc // indirect
	github.com/docker/go-connections v0.4.0 // indirect
	github.com/docker/go-units v0.4.0 // indirect
	github.com/gorilla/mux v1.7.1
	github.com/hashicorp/vault/api v1.0.1
	github.com/opencontainers/go-digest v1.0.0-rc1 // indirect
	github.com/opencontainers/image-spec v1.0.1 // indirect
	github.com/pkg/errors v0.8.1
	github.com/sirupsen/logrus v1.4.1
	github.com/stretchr/testify v1.3.0
	golang.org/x/net v0.0.0-20190424112056-4829fb13d2c6 // indirect
)

replace github.com/docker/docker v0.0.0-20190408150954-50ebe4562dfc => github.com/docker/engine v0.0.0-20190408150954-50ebe4562dfc
