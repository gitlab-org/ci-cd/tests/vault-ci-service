## Vault CI Service

This project provides a dockerized service of [Hashicorp Vault](https://www.vaultproject.io/).

The goal is to have a Vault Server with a - dynamically - predefined configuration of different
authentication methods, secret engines and secrets, so it can be next used for automated
integration tests in CI environment.

The service is ready to be used as GitLab CI service for the Docker executor. Other CI services
may try to use it, but supporting them is out of scope of this project.

## Configuration with GitLab CI

First, ensure that the job will be executed with
[`docker`](https://docs.gitlab.com/runner/executors/docker.html) or 
[`docker+machine`](https://docs.gitlab.com/runner/executors/docker_machine.html) executor. If you
have multiple Runners available for your project, then you will need to find out which of them
are using Docker executor and tag the job with the right [Runner tag](https://docs.gitlab.com/ce/ci/yaml/README.html#tags). 

Vault server within this service is started with `disable_mlock = true` setting, so **the privileged
mode is not required for the service to be started**.

To use the service within your CI job, prepare the proper configuration. You can use
the following template as a reference:

```yaml
job with vault:
  services:
  - name: registry.gitlab.com/gitlab-org/ci-cd/tests/vault-ci-service:1.1.2-latest
    alias: vault
  image: alpine:latest
  before_script:
  - apk add -U curl jq
  script: |
    curl -s -k https://vault:8443/metadata | jq .
    export rootToken=$(curl -s -k https://vault:8443/metadata | jq --raw-output .RootToken)
    curl -s -k -H "X-Vault-Token: ${rootToken}" https://vault:8443/v1/sys/auth
```

The above configuration will:

1. Setup this Vault CI service, started from the `registry.gitlab.com/gitlab-org/ci-cd/tests/vault-ci-service:1.1.2-latest`
   Docker image. It will also set the alias to `vault` using the
   [extended Docker configuration syntax](https://docs.gitlab.com/ce/ci/docker/using_docker_images.html#extended-docker-configuration-options).
   This will allow you later to use `vault` as the hostname of Vault's server.

1. Start the job container from `alpine:latest` image, with `curl` and `jq` installed. This is done only for
   the presentation purpose. For your case you can use whatever image and software to interact with Vault.
   Just use what you need or what you want to test.

1. Execute an example script that interacts with Vault:

    1. In first step the script uses `curl` to read metadata provided by the service. The metadata endpoint
       is provided by Vault CI Service and it's the only endpoint managed by this service. The expected output
       is something similar to:

        ```json
        {
          "URL": {
            "Scheme": "https",
            "Opaque": "",
            "User": null,
            "Host": "127.0.0.1:8200",
            "Path": "/",
            "RawPath": "",
            "ForceQuery": false,
            "RawQuery": "",
            "Fragment": ""
          },
          "RootToken": "s.YMzUbfO4TfhS9O0umA6EJeRt",
          "CA": {
            "CACert": {
              "CertificatePEM": "-----BEGIN CERTIFICATE-----\nMIIDADCCAeigAwIBAgIEXMd6TzANBgkqhkiG9w0BAQsFADAgMR4wHAYDVQQDExVW\nYXVsdCBUZXN0IFNlcnZpY2UgQ0EwHhcNMTkwNDI5MjIyNzI2WhcNMjkwNDI5MjIy\nNzI2WjAgMR4wHAYDVQQDExVWYXVsdCBUZXN0IFNlcnZpY2UgQ0EwggEiMA0GCSqG\nSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCcM9Wz5ocIlOBXWdi4qfJJ/P8FLJOybHC3\nxyFkhMAvj2cdr6KtHNE0Sci2cP5rX8bhSa4oSC4Jc/7mckOQQFzpJ7GC7/XFNg6G\n5zXN/gPDC46unhZ9RW14nUdo285S93IIQ8ZatmaWycjuMa06bO8MCu3Cby1U4ddR\n4UvoMqEbc0zn+38ryQAFyMEY27UjjC+pe6dMMpJNJYnnnBEKq04EaViVZoIeL/63\nosBnmSy6Rf3CcRbGkYvLw16iTPesf4TEI3sAM6AUPWeQ0x58QLNMLSbO+SDTg270\nINoELosfXs4Jv+WnYav3Mqpv6Lpa3XN5/T4WqcQBtoO0Y0ndSOJ3AgMBAAGjQjBA\nMA4GA1UdDwEB/wQEAwIChDAdBgNVHSUEFjAUBggrBgEFBQcDAgYIKwYBBQUHAwEw\nDwYDVR0TAQH/BAUwAwEB/zANBgkqhkiG9w0BAQsFAAOCAQEAaSkOsXwb5ZdSJDMq\nZxlzfISUYBNsZvrnFN2VjhhNaz8i49FQ8ev3CN7zHKeN+QhkDx42iO9V9UbEZkNj\nc5AXcqadBPue+VFtLAHs3ZkTpGo0QziIEWgV1PZOmeFrCOEcudPeAv903+pomxuM\nZGakysc5tUDdKFMbHVNBup6ktg2h8/oUkXlXYCHlH32kzpDW5nmtvLFDCrl0B/1B\n0iUy5ETvA98Ca4durxXDeyzc2034TpYbjH2gqBh0gVKX+kdXWNzGiqSXPoaUrers\neBClW9C2mPWbeZgDRGOM4kgpwMiL5JkbMVtia4twdo1lMDm7v/ADArRNCfjDGkTE\nftYZdQ==\n-----END CERTIFICATE-----\n",
              "PrivateKeyPEM": "-----BEGIN RSA PRIVATE KEY-----\nMIIEogIBAAKCAQEAnDPVs+aHCJTgV1nYuKnySfz/BSyTsmxwt8chZITAL49nHa+i\nrRzRNEnItnD+a1/G4UmuKEguCXP+5nJDkEBc6Sexgu/1xTYOhuc1zf4DwwuOrp4W\nfUVteJ1HaNvOUvdyCEPGWrZmlsnI7jGtOmzvDArtwm8tVOHXUeFL6DKhG3NM5/t/\nK8kABcjBGNu1I4wvqXunTDKSTSWJ55wRCqtOBGlYlWaCHi/+t6LAZ5ksukX9wnEW\nxpGLy8Neokz3rH+ExCN7ADOgFD1nkNMefECzTC0mzvkg04Nu9CDaBC6LH17OCb/l\np2Gr9zKqb+i6Wt1zef0+FqnEAbaDtGNJ3UjidwIDAQABAoIBAANPge7/Kst+xEZ1\nrAc16uGwkAMfD75PWBA9EzMbMSuQ4YGakFsU6kYubieXu3yxGfj6Y8uMxBUFNVjT\nASWUh6OVaMi6pz3XyHUJf3VvNcszeoGu7hEXoJtW0gWh6vyNLAiKzzBq/z+g5TZP\nLTm4x1Q9Aw8E5jQPWU3t9XrlX87CGVweRIiXvszpnIc4DaLDlwy3qVDoew2AccJe\nHQO5aRuZ7c45VjqY6OrJPys0eVXNVWJQDIaG7wWmsa9uNzjYNlBBbSHQJZ0Jju23\nFn+qSHBq444lDx7I7K0Q6TsKUmUySs7Fjz6WRoy/OxH4wOs9bDxaDa3F/xLXsNRS\nZdOjqqECgYEAxkyBtPxfPrlMjPWuuvpltfyU0P9HvpW519KgJwAi+h3H0wbDPxK7\n0qaRY1nbtQ1ky6dBWo52cIfMOFYCRz4FeY+4ZPkzJYFmreeVBK3qaFpkHKZq+cfY\nVNT95fq/p9Ti2dB8mloBskfrZDJi3iURqxOxFaRhxewhVgQRoKCd1zECgYEAyaeG\nM9Fds+hU3YuR0aWy80KcXyJhV66qrg/Y02bKhjbMBW/VIAn0K2KvUkfcsyewIt/C\nqAYTFIXQkVs9HVhHswVIu9ujgqYov1L8FIG00nbLEnIqBqL5h/gj0fMHx+sSWKiD\n+3iXPd+bWex2B7UWhegt7SHMuHJU6plsW6M8OicCgYAH9rqe0iPhGQACCTvjNnsv\nO92eJyJyfQDhsgvMhSzibp+/0d21gsMYSqg8HTe9pbQOmcD+KgqHqmyBNrMQuWTu\n5N6672cmcnP1N/+8GF/5oOe0Xtqc/XGqtoMb4V4hF2Ok68KVUFgRGOJTO9LMqSB0\nhm3uOJPUpw8PrUon8UtlgQKBgBv2nyWQYu4PybTvJclssOQK2KRYoCcIkaKRj//A\nwVTbRG98kpFez/00Mhu222P8nPA5F0U1km/GHqYJgPAQzeCFWxCx5Hq5j+z4FPzC\n/9AvqPll224QK4ovXsg+a1XNwz2IIMU+c7qgx2ow4C0xb24iRDwd88WgFSZzIBOV\nD0UBAoGAXckWb7PkShllHAefz3Lu5MaRXF02RCmyyfui5XCfJieDzO/kSxM6GReV\nSN8vmUWDhLKilniiY0HL4Ur5OqU1LIM9tK7rWdiIYnM2PXpwlkm0Nr5P25lRKOVG\nklBlLy7hThYwC968CbOLqEKHgWO77SOR8Y5Sp24aKCwkyVXFqiU=\n-----END RSA PRIVATE KEY-----\n"
            },
            "Certificates": {
              "cert1": {
                "CertificatePEM": "-----BEGIN CERTIFICATE-----\nMIIC8DCCAdigAwIBAgIEXMd6UTANBgkqhkiG9w0BAQsFADAgMR4wHAYDVQQDExVW\nYXVsdCBUZXN0IFNlcnZpY2UgQ0EwHhcNMTkwNDI5MjIyNzI4WhcNMjAwNDI5MjIy\nNzI4WjAQMQ4wDAYDVQQDEwVjZXJ0MTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCC\nAQoCggEBAN159i0Vygnj57pTI+T+hu/xpoqQeB/QVnWDMuFo8/0BJrogkC8vdk02\nw5kbptrYs+1EXhhk7cKiwYw+7gv3hunxJTbd4ZrILDWKYx2IxhPyiFJTnMlDfGpf\nRd1UT4dydAOJBC80M5RVgQQK+F254/nGYO/8bmNCeV2EBWRqFecWhFHP76XhvMZm\ngjNgnvZwYNEFTT66tKJAmCHgQX+OrebFAaq4eK1NLNixc7Ovl4XwGZGcUMj/LYaE\nRyxhnDQCmICX4bDKnL0rSj17SXOtK9OvFDWdpyWYXC7B5OZybYv97OAacf5SOuim\n/pzBO/Y6s5QGFku9eWC4BwDp/+920jUCAwEAAaNCMEAwDgYDVR0PAQH/BAQDAgeA\nMB0GA1UdJQQWMBQGCCsGAQUFBwMCBggrBgEFBQcDATAPBgNVHREECDAGhwR/AAAB\nMA0GCSqGSIb3DQEBCwUAA4IBAQAA1KVMc+p9gZjvkrx2EpArHyZGQmVAml7t2Z9S\nqu63vDbI+0q8KETg0yuy329TWShq80HZdN65kVBeKjrMqMLIQfDs09G6+WiwsEZV\nZXFW10ubft0+NjOeJY6vyct9XVgovu3mS1dYsbzyC5ULMke9NKQANZ3dfuvBPJti\n33G3BIPB5tyAhcy4qxSbmD5Jvvdz1lXA468laxbN6nQS3VBkFKZBgMuXVRMMRopg\nMjT+NHzH+10JJ4vnKvqQVwFtu9UvKHJMKHSyNA62AVGcBtMpe++2wrlTMRJ4//n3\nrU6OVvjDnPnh8WAaBUOS/Cjs0a75ijUbDtvomPtUzT7AhBpQ\n-----END CERTIFICATE-----\n",
                "PrivateKeyPEM": "-----BEGIN RSA PRIVATE KEY-----\nMIIEpAIBAAKCAQEA3Xn2LRXKCePnulMj5P6G7/GmipB4H9BWdYMy4Wjz/QEmuiCQ\nLy92TTbDmRum2tiz7UReGGTtwqLBjD7uC/eG6fElNt3hmsgsNYpjHYjGE/KIUlOc\nyUN8al9F3VRPh3J0A4kELzQzlFWBBAr4Xbnj+cZg7/xuY0J5XYQFZGoV5xaEUc/v\npeG8xmaCM2Ce9nBg0QVNPrq0okCYIeBBf46t5sUBqrh4rU0s2LFzs6+XhfAZkZxQ\nyP8thoRHLGGcNAKYgJfhsMqcvStKPXtJc60r068UNZ2nJZhcLsHk5nJti/3s4Bpx\n/lI66Kb+nME79jqzlAYWS715YLgHAOn/73bSNQIDAQABAoIBAQDDZ325nYXaYAft\nrrj2q1KmlNBZZMl3WDtQBud3VUAfs0pV4bEU0I1R5sWAapM2dweYiT9zymiz1yFo\n+YTRbhzMfwRUekM4avAGdQip4CjTJKL97+Ne93nn6BMeCer5GBQfol3cv/PaJe5k\ngMFNAFuR3mNcmZ1TWiz2Qyr2w1p7vQ8SXE+D9X6cfQdJS6wJxflJHxx0ur2gyI90\n3nFj6x8DyKXLus/Mwwadexm7UuALRNvLECyBnWZPiU5MdwSQ9uKgRBfaGEdtbmgD\ny4///hk8YtZx8VzpTaR37lAHrYzwqdHqeSht2DPjHZRPy9+DSAQi4o4eiiDdQsCu\n0yOLjq/pAoGBAN+EswwDZsbaY7TDfi9cQ6CTACnrSeflttz8v/pGlWW26pvfD+l3\nvLrbLxpVrVSefmCplIpqFBX5XtK9UQQoIvfjws8q31BWezT1GxqVt27UnpjsiodT\nzlvvDhLfqY1zogSBFimcz+MU5yY3LXdIap30djZ08EsiG5lxCnrLgJpjAoGBAP2p\nTE6XFGuDYnqrj7V4ARVbtVT3aGUew1bL6jEy/FUuhH2YcJcuLz/TZ/KbGFtVbx+G\nSGYaTZ6HeNvJomRRI05flKvQ38WLXW9sXQOT/J8wZxzmKvmpT76KH4wwZSHycstw\n/LU6YI3zUajAiFJ3EyTnEet4KSPVhkGC7aIHzXiHAoGAHxZavLIAamnXcj26YCXp\n9fxcCniCF/G4JDY3ET0d7D6rlLBKi0MvCaIQhA4khF9i3ljXowSr9H5xdMgF65kV\ne/q+joe89XSBwFTSxzjJgW2q/UPw5G+AhQLTp0ZaU6UghJXbmkAIHeI7X/JOrYdx\n5LQqeNp8zUZaJlY1ieyh31cCgYEAry/TgZuaAL7Wrr36HGxq4yNZUvsj4GKkqjde\n4OfDmdjsrAkyCVdeTohlDArNgZa5jl4hdlLINKp/b9wMCZh399LPTPKO+VHND/0Z\nKDV2jULSlATqvU6PwpqGOz3ZOt7FJXg9L7THpoHbbd66x6lxUVU87REp6JO5i1kv\nYW5eG9UCgYA1RMqal4HXOQXHwU4vh94AN85/0OSd8rpSqgTnTFLpN0mYIQASzc2C\n8GfVhTBMOghIgmtcrJQIMOsCScY2rxe4i55T2MPiyA6q+Do1SvzfkBeW+d5EX826\nbg7wosroG/uSC14UNLneGfrA7oiUBxrlCQhXpDLBadP5GFhjVYvDug==\n-----END RSA PRIVATE KEY-----\n"
              },
              "vault": {
                "CertificatePEM": "-----BEGIN CERTIFICATE-----\nMIIC8DCCAdigAwIBAgIEXMd6UDANBgkqhkiG9w0BAQsFADAgMR4wHAYDVQQDExVW\nYXVsdCBUZXN0IFNlcnZpY2UgQ0EwHhcNMTkwNDI5MjIyNzI2WhcNMjAwNDI5MjIy\nNzI2WjAQMQ4wDAYDVQQDEwV2YXVsdDCCASIwDQYJKoZIhvcNAQEBBQADggEPADCC\nAQoCggEBAKAKmoxyMB6WpGIrTRPTk8mv8FKRgXw5iq4QIqZ9Qiq+l9B2zhU4Clry\nZoO4glMjtxBx5B0qT4VpgjWw8l9TZI38vDa2p32FnCVfdD+pOQar/zW8Z0Wwzr/Y\nioOb71KtAi5h8OuP2WByxbDLoaAVO7xMAvm5DliO8ouoDEpBqyzqtZhjBswvJbOn\nXTmPLjPRNhTrX33X+Pcky0EXzqplVGSPuYdHG967SW25dy35Rk6F8zMhH143gl2j\nZbnPmaNryYD5vwdElCZphY5SPlXhCMep3ZxqFyUC9wE1TP979zBP0iotnIp7bn6s\nVL+wsOdiisLreQxdJ/X03LVBtDt4LXMCAwEAAaNCMEAwDgYDVR0PAQH/BAQDAgeA\nMB0GA1UdJQQWMBQGCCsGAQUFBwMCBggrBgEFBQcDATAPBgNVHREECDAGhwR/AAAB\nMA0GCSqGSIb3DQEBCwUAA4IBAQBClgbXX0T/kyDyZ5RNowQ4gK4OuOhR1i0x2Bbf\nWiT+IdBiM1UtDyg0XmkHh5VsESSH1cwxjN9yy+ugLVx/4EqgBCxM7g7Kke2ltUyA\nY2imLsbQBZD0n0ZaXLvCnXvAXnThj9u3FCwloCRrc+BXeLuPx/rK9z8ISD+pxxiq\nc0qIZCnnM7/SAaFC88mREqr0QVkn77qqvCf9b0RzBS2IdMdVv1nOZIkQRc/dpBnX\nJB39yAwJ8G8TMTrK5rndBJYdYOK7Bnhx6BhrpVrY527PSQu7j/Ua8PCVrWC9bMYH\n9Oc+wbSFuM+krQu+t/O+oiVV9nsmq/lyfCMjOXcb3JDd8Qva\n-----END CERTIFICATE-----\n",
                "PrivateKeyPEM": "-----BEGIN RSA PRIVATE KEY-----\nMIIEowIBAAKCAQEAoAqajHIwHpakYitNE9OTya/wUpGBfDmKrhAipn1CKr6X0HbO\nFTgKWvJmg7iCUyO3EHHkHSpPhWmCNbDyX1Nkjfy8NranfYWcJV90P6k5Bqv/Nbxn\nRbDOv9iKg5vvUq0CLmHw64/ZYHLFsMuhoBU7vEwC+bkOWI7yi6gMSkGrLOq1mGMG\nzC8ls6ddOY8uM9E2FOtffdf49yTLQRfOqmVUZI+5h0cb3rtJbbl3LflGToXzMyEf\nXjeCXaNluc+Zo2vJgPm/B0SUJmmFjlI+VeEIx6ndnGoXJQL3ATVM/3v3ME/SKi2c\nintufqxUv7Cw52KKwut5DF0n9fTctUG0O3gtcwIDAQABAoIBAH7HZ56SZmQU2aua\nubhXBt/iq/H8qiKKQr2YsppI572Mth7Bvoc4mZtqIqN96l+KD17LmfUwq0X4StnV\njmrNWP7RkxeUoiHhUcyZTTGREEekwMsEsJqYcW+wTinrwS4xN2Ue2QcCMSUiRlDD\nPwndSDDl1gWU/zRQKPtQ6Et6sesl0iZDBO1+KD3QzGHlO/QrrDOH3Wat9hRc2v+N\n0TqCBZnfHjUHd0gKL2Agmiko2Rh/mdn1aNp/PTMSmTBtgS/GFR4CPNB15NV+s7lL\n5MCpeVrZEoilIXh/DFPlXd8Kee8qtmu8U9QM6itO3LMaQIsdsANe705E6+9OcwrE\nqNHbJcECgYEAwV1t9uGkBpCnMDgAt7qjOKXIIfz0poBDNIJjf2/hFObpeAI2bhYd\niYlh59Nq1IaqdHvmhQaZYfOmH/4fuJwmBvJ9F0zRODg2uuegwADJbWB3gSAHXLce\n+YfNUHPZNOsL8jCriLmWUQVHVG/bkyGmhqcUsSy5ihw7t/phPXupQgkCgYEA0+Hb\ngO7jJF0faHYPJ2RpdN4vNU4uJuHZwvHdZB1oTF8IsunO1olGULIGmcd1XSmMO0+A\nzpycxGOtuTkUe+p/oFWvO9fx0vgF+OeCQ4MeEQ5i9G6y4Hga6vpp5BruHr/EzdD7\nKo5cl9yQBQQn8Jjo/c+/kmpHeWEeLccCNsV4IpsCgYAqFeOxMDs7EpDQ9d9yHbUY\nxzxf6WZ2NsRp0VdGjVs36Wo3LIQcQcP8A2LDykgeKZE/+RebLGyF3TnzDKRq0dGV\nAYIoaBfBXLrMw01BfOpaW/ympgn0LAcZ2GzfbdeApQfclqs+/uWwjK2RuybXRyOv\nev4N8eYQIUT+LgisERYtwQKBgHn41+KmIm4JiY4aAGk93ZNgYC7AfcL2WuRlVXxP\njX4Ba0GLZP2NcsP7Z99YuhBSXZ67Crxl3b0UYKpy2H7pJGVJTiIWEujOzKlXpF52\n+XT6Ii7yFtkvateAJjsg4Zkj7ghJWH1MDLODTgkFNnTDocSStoZz1gdix/u0R2r+\nTEUnAoGBAJKa2WL9eI0SELYLle7edVcrXepfS7I/yLPcFzaXn9WwgQuSP9VTjgjq\nXPXCXSim3tHwD0x2yMkZjovslSDEaueqx6cWddBoMcTAa6DEJ/aOnGm0THgyeqWG\nZ+wpLktLWyRrsoLA1PQvta86zVA5tEZ2EPfoIyGCDbHe15J76mTE\n-----END RSA PRIVATE KEY-----\n"
              }
            }
          },
          "AuthMethods": {
            "Userpass": {
              "Username": "testuser",
              "Password": "testpass"
            },
            "TLSCert": {
              "CA": {
                "CertificatePEM": "-----BEGIN CERTIFICATE-----\nMIIDADCCAeigAwIBAgIEXMd6TzANBgkqhkiG9w0BAQsFADAgMR4wHAYDVQQDExVW\nYXVsdCBUZXN0IFNlcnZpY2UgQ0EwHhcNMTkwNDI5MjIyNzI2WhcNMjkwNDI5MjIy\nNzI2WjAgMR4wHAYDVQQDExVWYXVsdCBUZXN0IFNlcnZpY2UgQ0EwggEiMA0GCSqG\nSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCcM9Wz5ocIlOBXWdi4qfJJ/P8FLJOybHC3\nxyFkhMAvj2cdr6KtHNE0Sci2cP5rX8bhSa4oSC4Jc/7mckOQQFzpJ7GC7/XFNg6G\n5zXN/gPDC46unhZ9RW14nUdo285S93IIQ8ZatmaWycjuMa06bO8MCu3Cby1U4ddR\n4UvoMqEbc0zn+38ryQAFyMEY27UjjC+pe6dMMpJNJYnnnBEKq04EaViVZoIeL/63\nosBnmSy6Rf3CcRbGkYvLw16iTPesf4TEI3sAM6AUPWeQ0x58QLNMLSbO+SDTg270\nINoELosfXs4Jv+WnYav3Mqpv6Lpa3XN5/T4WqcQBtoO0Y0ndSOJ3AgMBAAGjQjBA\nMA4GA1UdDwEB/wQEAwIChDAdBgNVHSUEFjAUBggrBgEFBQcDAgYIKwYBBQUHAwEw\nDwYDVR0TAQH/BAUwAwEB/zANBgkqhkiG9w0BAQsFAAOCAQEAaSkOsXwb5ZdSJDMq\nZxlzfISUYBNsZvrnFN2VjhhNaz8i49FQ8ev3CN7zHKeN+QhkDx42iO9V9UbEZkNj\nc5AXcqadBPue+VFtLAHs3ZkTpGo0QziIEWgV1PZOmeFrCOEcudPeAv903+pomxuM\nZGakysc5tUDdKFMbHVNBup6ktg2h8/oUkXlXYCHlH32kzpDW5nmtvLFDCrl0B/1B\n0iUy5ETvA98Ca4durxXDeyzc2034TpYbjH2gqBh0gVKX+kdXWNzGiqSXPoaUrers\neBClW9C2mPWbeZgDRGOM4kgpwMiL5JkbMVtia4twdo1lMDm7v/ADArRNCfjDGkTE\nftYZdQ==\n-----END CERTIFICATE-----\n",
                "PrivateKeyPEM": "-----BEGIN RSA PRIVATE KEY-----\nMIIEogIBAAKCAQEAnDPVs+aHCJTgV1nYuKnySfz/BSyTsmxwt8chZITAL49nHa+i\nrRzRNEnItnD+a1/G4UmuKEguCXP+5nJDkEBc6Sexgu/1xTYOhuc1zf4DwwuOrp4W\nfUVteJ1HaNvOUvdyCEPGWrZmlsnI7jGtOmzvDArtwm8tVOHXUeFL6DKhG3NM5/t/\nK8kABcjBGNu1I4wvqXunTDKSTSWJ55wRCqtOBGlYlWaCHi/+t6LAZ5ksukX9wnEW\nxpGLy8Neokz3rH+ExCN7ADOgFD1nkNMefECzTC0mzvkg04Nu9CDaBC6LH17OCb/l\np2Gr9zKqb+i6Wt1zef0+FqnEAbaDtGNJ3UjidwIDAQABAoIBAANPge7/Kst+xEZ1\nrAc16uGwkAMfD75PWBA9EzMbMSuQ4YGakFsU6kYubieXu3yxGfj6Y8uMxBUFNVjT\nASWUh6OVaMi6pz3XyHUJf3VvNcszeoGu7hEXoJtW0gWh6vyNLAiKzzBq/z+g5TZP\nLTm4x1Q9Aw8E5jQPWU3t9XrlX87CGVweRIiXvszpnIc4DaLDlwy3qVDoew2AccJe\nHQO5aRuZ7c45VjqY6OrJPys0eVXNVWJQDIaG7wWmsa9uNzjYNlBBbSHQJZ0Jju23\nFn+qSHBq444lDx7I7K0Q6TsKUmUySs7Fjz6WRoy/OxH4wOs9bDxaDa3F/xLXsNRS\nZdOjqqECgYEAxkyBtPxfPrlMjPWuuvpltfyU0P9HvpW519KgJwAi+h3H0wbDPxK7\n0qaRY1nbtQ1ky6dBWo52cIfMOFYCRz4FeY+4ZPkzJYFmreeVBK3qaFpkHKZq+cfY\nVNT95fq/p9Ti2dB8mloBskfrZDJi3iURqxOxFaRhxewhVgQRoKCd1zECgYEAyaeG\nM9Fds+hU3YuR0aWy80KcXyJhV66qrg/Y02bKhjbMBW/VIAn0K2KvUkfcsyewIt/C\nqAYTFIXQkVs9HVhHswVIu9ujgqYov1L8FIG00nbLEnIqBqL5h/gj0fMHx+sSWKiD\n+3iXPd+bWex2B7UWhegt7SHMuHJU6plsW6M8OicCgYAH9rqe0iPhGQACCTvjNnsv\nO92eJyJyfQDhsgvMhSzibp+/0d21gsMYSqg8HTe9pbQOmcD+KgqHqmyBNrMQuWTu\n5N6672cmcnP1N/+8GF/5oOe0Xtqc/XGqtoMb4V4hF2Ok68KVUFgRGOJTO9LMqSB0\nhm3uOJPUpw8PrUon8UtlgQKBgBv2nyWQYu4PybTvJclssOQK2KRYoCcIkaKRj//A\nwVTbRG98kpFez/00Mhu222P8nPA5F0U1km/GHqYJgPAQzeCFWxCx5Hq5j+z4FPzC\n/9AvqPll224QK4ovXsg+a1XNwz2IIMU+c7qgx2ow4C0xb24iRDwd88WgFSZzIBOV\nD0UBAoGAXckWb7PkShllHAefz3Lu5MaRXF02RCmyyfui5XCfJieDzO/kSxM6GReV\nSN8vmUWDhLKilniiY0HL4Ur5OqU1LIM9tK7rWdiIYnM2PXpwlkm0Nr5P25lRKOVG\nklBlLy7hThYwC968CbOLqEKHgWO77SOR8Y5Sp24aKCwkyVXFqiU=\n-----END RSA PRIVATE KEY-----\n"
              },
              "AuthCert": {
                "CertificatePEM": "-----BEGIN CERTIFICATE-----\nMIIC8DCCAdigAwIBAgIEXMd6UTANBgkqhkiG9w0BAQsFADAgMR4wHAYDVQQDExVW\nYXVsdCBUZXN0IFNlcnZpY2UgQ0EwHhcNMTkwNDI5MjIyNzI4WhcNMjAwNDI5MjIy\nNzI4WjAQMQ4wDAYDVQQDEwVjZXJ0MTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCC\nAQoCggEBAN159i0Vygnj57pTI+T+hu/xpoqQeB/QVnWDMuFo8/0BJrogkC8vdk02\nw5kbptrYs+1EXhhk7cKiwYw+7gv3hunxJTbd4ZrILDWKYx2IxhPyiFJTnMlDfGpf\nRd1UT4dydAOJBC80M5RVgQQK+F254/nGYO/8bmNCeV2EBWRqFecWhFHP76XhvMZm\ngjNgnvZwYNEFTT66tKJAmCHgQX+OrebFAaq4eK1NLNixc7Ovl4XwGZGcUMj/LYaE\nRyxhnDQCmICX4bDKnL0rSj17SXOtK9OvFDWdpyWYXC7B5OZybYv97OAacf5SOuim\n/pzBO/Y6s5QGFku9eWC4BwDp/+920jUCAwEAAaNCMEAwDgYDVR0PAQH/BAQDAgeA\nMB0GA1UdJQQWMBQGCCsGAQUFBwMCBggrBgEFBQcDATAPBgNVHREECDAGhwR/AAAB\nMA0GCSqGSIb3DQEBCwUAA4IBAQAA1KVMc+p9gZjvkrx2EpArHyZGQmVAml7t2Z9S\nqu63vDbI+0q8KETg0yuy329TWShq80HZdN65kVBeKjrMqMLIQfDs09G6+WiwsEZV\nZXFW10ubft0+NjOeJY6vyct9XVgovu3mS1dYsbzyC5ULMke9NKQANZ3dfuvBPJti\n33G3BIPB5tyAhcy4qxSbmD5Jvvdz1lXA468laxbN6nQS3VBkFKZBgMuXVRMMRopg\nMjT+NHzH+10JJ4vnKvqQVwFtu9UvKHJMKHSyNA62AVGcBtMpe++2wrlTMRJ4//n3\nrU6OVvjDnPnh8WAaBUOS/Cjs0a75ijUbDtvomPtUzT7AhBpQ\n-----END CERTIFICATE-----\n",
                "PrivateKeyPEM": "-----BEGIN RSA PRIVATE KEY-----\nMIIEpAIBAAKCAQEA3Xn2LRXKCePnulMj5P6G7/GmipB4H9BWdYMy4Wjz/QEmuiCQ\nLy92TTbDmRum2tiz7UReGGTtwqLBjD7uC/eG6fElNt3hmsgsNYpjHYjGE/KIUlOc\nyUN8al9F3VRPh3J0A4kELzQzlFWBBAr4Xbnj+cZg7/xuY0J5XYQFZGoV5xaEUc/v\npeG8xmaCM2Ce9nBg0QVNPrq0okCYIeBBf46t5sUBqrh4rU0s2LFzs6+XhfAZkZxQ\nyP8thoRHLGGcNAKYgJfhsMqcvStKPXtJc60r068UNZ2nJZhcLsHk5nJti/3s4Bpx\n/lI66Kb+nME79jqzlAYWS715YLgHAOn/73bSNQIDAQABAoIBAQDDZ325nYXaYAft\nrrj2q1KmlNBZZMl3WDtQBud3VUAfs0pV4bEU0I1R5sWAapM2dweYiT9zymiz1yFo\n+YTRbhzMfwRUekM4avAGdQip4CjTJKL97+Ne93nn6BMeCer5GBQfol3cv/PaJe5k\ngMFNAFuR3mNcmZ1TWiz2Qyr2w1p7vQ8SXE+D9X6cfQdJS6wJxflJHxx0ur2gyI90\n3nFj6x8DyKXLus/Mwwadexm7UuALRNvLECyBnWZPiU5MdwSQ9uKgRBfaGEdtbmgD\ny4///hk8YtZx8VzpTaR37lAHrYzwqdHqeSht2DPjHZRPy9+DSAQi4o4eiiDdQsCu\n0yOLjq/pAoGBAN+EswwDZsbaY7TDfi9cQ6CTACnrSeflttz8v/pGlWW26pvfD+l3\nvLrbLxpVrVSefmCplIpqFBX5XtK9UQQoIvfjws8q31BWezT1GxqVt27UnpjsiodT\nzlvvDhLfqY1zogSBFimcz+MU5yY3LXdIap30djZ08EsiG5lxCnrLgJpjAoGBAP2p\nTE6XFGuDYnqrj7V4ARVbtVT3aGUew1bL6jEy/FUuhH2YcJcuLz/TZ/KbGFtVbx+G\nSGYaTZ6HeNvJomRRI05flKvQ38WLXW9sXQOT/J8wZxzmKvmpT76KH4wwZSHycstw\n/LU6YI3zUajAiFJ3EyTnEet4KSPVhkGC7aIHzXiHAoGAHxZavLIAamnXcj26YCXp\n9fxcCniCF/G4JDY3ET0d7D6rlLBKi0MvCaIQhA4khF9i3ljXowSr9H5xdMgF65kV\ne/q+joe89XSBwFTSxzjJgW2q/UPw5G+AhQLTp0ZaU6UghJXbmkAIHeI7X/JOrYdx\n5LQqeNp8zUZaJlY1ieyh31cCgYEAry/TgZuaAL7Wrr36HGxq4yNZUvsj4GKkqjde\n4OfDmdjsrAkyCVdeTohlDArNgZa5jl4hdlLINKp/b9wMCZh399LPTPKO+VHND/0Z\nKDV2jULSlATqvU6PwpqGOz3ZOt7FJXg9L7THpoHbbd66x6lxUVU87REp6JO5i1kv\nYW5eG9UCgYA1RMqal4HXOQXHwU4vh94AN85/0OSd8rpSqgTnTFLpN0mYIQASzc2C\n8GfVhTBMOghIgmtcrJQIMOsCScY2rxe4i55T2MPiyA6q+Do1SvzfkBeW+d5EX826\nbg7wosroG/uSC14UNLneGfrA7oiUBxrlCQhXpDLBadP5GFhjVYvDug==\n-----END RSA PRIVATE KEY-----\n"
              }
            }
          },
          "TestSecrets": [
            {
              "Type": "kv1",
              "Path": "kv1/my-secrets",
              "Data": {
                "keyBool": false,
                "keyInt": 2,
                "keyString": "value1"
              }
            },
            {
              "Type": "kv2",
              "Path": "kv2/data/my-secrets",
              "Data": {
                "data": {
                  "keyBool": false,
                  "keyInt": 2,
                  "keyString": "value1"
                }
              }
            }
          ]
        }
        ```

        The metadata contain information about predefined configuration, credentials and secrets prepared for CI
        usage.

    1. In second step the script will extract the `RootToken` from the metadata and export it as a `rootToken` variable.

    1. In the last step the script will use the token stored in `rootToken` to request a path that requires authentication.
       For this the `/v1/sys/auth` path is used. This request is proxied to the Vault Server.

### Notices

1. Please notice that the example script uses `curl -k` option which disabled the TLS certificates verification. This is
   used because both Vault server and Vault CI Service metadata and proxy server are using a local Certificates Authority
   with a self-signed root certificate.

   If the software that you want to use against Vault doesn't allow to configure insecure connections, you can get
   certificates from the metadata, using:

   - for the CA certificate: `.CA.CACert.CertificatePEM` value,
   - for the Vault server certificate (as well as for the Vault CI Service proxy and metadata server):
     `.CA.Certificates.vault.CertificatePEM` value.

1. If you want to use TLS Client Authentication, then you need to talk to Vault server directly, not through the
   Vault CI Service proxy. For that just change the port number from `8443` to `8200`, so for example:

   ```bash
   curl -s -k --cacert /some/ca.cert --cert /some/auth.cert --key /some/auth.key -X POST https://vault:8200/v1/auth/cert/login | jq .
   ```

## Authors

Tomasz Maczukin, 2019, GitLab Inc.

## License

MIT
