#!/bin/bash

set -eo pipefail

hostname=$1
if [[ $# -le 0 ]]; then
    hostname="127.0.0.1"
fi

cmd() {
    echo
    echo -e "\033[37;1m---"
    echo "exec: $@"
    echo -e "\033[0;0m"

    eval "$@"
}

cmd "curl -s -k https://${hostname}:8443/metadata | jq ."

export secret1Path=$(curl -s -k https://${hostname}:8443/metadata | jq --raw-output .TestSecrets[0].Path)
export secret2Path=$(curl -s -k https://${hostname}:8443/metadata | jq --raw-output .TestSecrets[1].Path)

export rootToken=$(curl -s -k https://${hostname}:8443/metadata | jq --raw-output .RootToken)
cmd "curl -s -k -H \"X-Vault-Token: ${rootToken}\" https://${hostname}:8443/v1/sys/auth | jq ."
cmd "curl -s -k -H \"X-Vault-Token: ${rootToken}\" https://${hostname}:8443/v1/${secret1Path} | jq .data"
cmd "curl -s -k -H \"X-Vault-Token: ${rootToken}\" https://${hostname}:8443/v1/${secret2Path} | jq .data.data"

export user=$(curl -s -k https://${hostname}:8443/metadata | jq --raw-output .AuthMethods.Userpass.Username)
export pass=$(curl -s -k https://${hostname}:8443/metadata | jq --raw-output .AuthMethods.Userpass.Password)
cmd "curl -s -k -X POST https://${hostname}:8443/v1/auth/userpass/login/${user} --data \"{\\\"password\\\": \\\"${pass}\\\"}\" | jq ."

export userpassToken=$(curl -s -k -X POST https://${hostname}:8443/v1/auth/userpass/login/${user} --data "{\"password\": \"${pass}\"}" | jq --raw-output .auth.client_token)
cmd "curl -s -k -H \"X-Vault-Token: ${userpassToken}\" https://${hostname}:8443/v1/${secret1Path} | jq .data"
cmd "curl -s -k -H \"X-Vault-Token: ${userpassToken}\" https://${hostname}:8443/v1/${secret2Path} | jq .data.data"

cmd "curl -s -k https://${hostname}:8443/metadata | jq .AuthMethods.TLSCert.CA.CertificatePEM --raw-output > /tmp/ca.cert"
cmd "curl -s -k https://${hostname}:8443/metadata | jq .AuthMethods.TLSCert.AuthCert.CertificatePEM --raw-output > /tmp/auth.cert"
cmd "curl -s -k https://${hostname}:8443/metadata | jq .AuthMethods.TLSCert.AuthCert.PrivateKeyPEM --raw-output > /tmp/auth.key"

cmd "curl -s -k --cacert /tmp/ca.cert --cert /tmp/auth.cert --key /tmp/auth.key -X POST https://${hostname}:8200/v1/auth/cert/login | jq ."

export certToken=$(curl -s -k --cacert /tmp/ca.cert --cert /tmp/auth.cert --key /tmp/auth.key -X POST https://${hostname}:8200/v1/auth/cert/login | jq --raw-output .auth.client_token)
cmd "curl -s -k -H \"X-Vault-Token: ${certToken}\" https://${hostname}:8443/v1/${secret1Path} | jq .data"
cmd "curl -s -k -H \"X-Vault-Token: ${certToken}\" https://${hostname}:8443/v1/${secret2Path} | jq .data.data"
